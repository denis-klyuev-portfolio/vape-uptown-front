import React from 'react';

import Flavor from '../components/Flavor';

export default ({flavors, vendors}) => (
	<div className="flavor-list">
		{flavors.map(li => <Flavor key={`flavor-${li.id}`} {...li}
								   vendor={vendors[li.vendorId]}/>)}
	</div>
);
