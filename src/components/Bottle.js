import React from 'react';
import './Bottle.scss';
import numeral from 'numeral';

const safeNumber = num => numeral(num).format('0[.]0');

const Bottle = props => (
	<div className="bottle">
		<div className="nic">
			<div>Nic</div>
			<div>{safeNumber(props.nic)}</div>
			<div>{props.measure}</div>
		</div>
		<div className="pg">
			<div>PG</div>
			<div>{safeNumber(props.pg)}</div>
			<div>{props.measure}</div>
		</div>
		<div className="vg">
			<div>VG</div>
			<div>{safeNumber(props.vg)}</div>
			<div>{props.measure}</div>
		</div>
		<div className="ad">
			<div>AD</div>
			<div>{safeNumber(props.ad)}</div>
			<div>{props.measure}</div>
		</div>
		<div className="fl">
			<div>FL</div>
			<div>{safeNumber(props.fl)}</div>
			<div>{props.measure}</div>
		</div>
	</div>
);

export default Bottle;
