import React from 'react';
import {Dropdown, DropdownItem} from 'muicss/react';

const SelectorView = ({label, list, onClick, getClassName}) => (
	<Dropdown color="primary" label={label}>
		{list.map(([id, name]) => {
				id = Number(id);
				return (
					<DropdownItem
						className={getClassName(name, id)}
						key={id}
						onClick={e => {onClick(e, name, id)}}
					>{name}
					</DropdownItem>
				);
			}
		)}
	</Dropdown>
);

export default SelectorView;
