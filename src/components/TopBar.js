import React from 'react';
import {connect} from 'react-redux';
import {Appbar} from 'muicss/react';
import LocaleSelector from '../containers/LocaleSelector';
import {Link} from 'react-router-dom';

const TopBar = ({locale, user}) => (
	<Appbar>
		<table width="100%">
			<tbody>
			<tr style={{verticalAlign: 'middle'}}>
				<td className="mui--appbar-height" style={{paddingLeft: '15px'}}>
					<Link to="/">{locale.topBar.homeLinkText}</Link>
				</td>
				<td className="mui--appbar-height" style={{textAlign: 'right'}}>
					<LocaleSelector/>
				</td>
				<td className="mui--appbar-height" style={{textAlign: 'right', paddingRight: '15px'}}>
					<Link to="/flavor-list">{locale.topBar.allFlavorsLinkText}</Link>
				</td>
				<td className="mui--appbar-height" style={{textAlign: 'right', paddingRight: '15px'}}>
					{!!user.data
						? <span>
							<Link to="/profile">{locale.topBar.welcome}<b>{user.data.name || 'noname'}</b></Link>
							<a href="/auth/logout">{locale.topBar.logout}</a>
						</span>
						: <a
							color="secondary"
							href="/auth/login/gg">{locale.topBar.needAuth}</a>
					}
				</td>
			</tr>
			</tbody>
		</table>
	</Appbar>
);

export default connect(({locale, user}) => ({locale, user}))(TopBar);
