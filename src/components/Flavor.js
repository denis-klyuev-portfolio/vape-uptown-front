import React from 'react';
import {Link} from 'react-router-dom';

const Flavor = ({id, name, vendor, clickable}) => {
	const flavorBody = <div className="flavor">
		<div className="name">{name}</div>
		<div className="vendor">{vendor.name}</div>
	</div>;

	return clickable ? (
		<Link to={`/flavor/${id}`}>
			{flavorBody}
		</Link>
	) : flavorBody;
};

Flavor.propTypes = {
	id: React.PropTypes.number.isRequired,
	name: React.PropTypes.string.isRequired,
	vendorId: React.PropTypes.number,
	vendor: React.PropTypes.object.isRequired,
	clickable: React.PropTypes.bool
};
Flavor.defaultProps = {
	clickable: true
};

export default Flavor;
