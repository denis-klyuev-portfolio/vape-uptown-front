class Api {
	constructor() {
		this.url = '/api';
	}

	/**
	 *
	 * @param {string} method название метода
	 * @param {object} params параметры вызова
	 * @returns {Promise}
	 */
	request(method, params = {}) {
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		const body = JSON.stringify({
			method,
			params
		});

		return fetch(this.url, {
			credentials: 'same-origin',
			method: 'POST',
			headers,
			body
		})
			.then(response => response.json())
			.then(({result, ...rest}) => rest);
	}
}

export default new Api();
