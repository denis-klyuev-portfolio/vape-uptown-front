import Api from '../Api';

//	general
export const addFlavor = data => ({
	type: 'ADD_FLAVOR',
	data
});
export const addVendor = data => ({
	type: 'ADD_VENDOR',
	data
});
export const addFlavors = data => ({
	type: 'ADD_FLAVORS',
	data
});
export const addVendors = data => ({
	type: 'ADD_VENDORS',
	data
});
//	liquid search
export const liquidSearchLiquidName = data => ({
	type: 'LIQUID_SEARCH_LIQUID_NAME',
	data
});
export const liquidSearchFlavorName = data => ({
	type: 'LIQUID_SEARCH_TYPE_HINT',
	data
});
export const liquidSearchStartSearch = () => ({
	type: 'LIQUID_SEARCH_START_SEARCH'
});
export const liquidSearchStopSearch = data => ({
	type: 'LIQUID_SEARCH_STOP_SEARCH',
	data
});
export const liquidSearchToggleHashtag = id => ({
	type: 'LIQUID_SEARCH_TOGGLE_HASHTAG',
	id
});
export const liquidSearchAddHashtag = id => ({
	type: 'LIQUID_SEARCH_ADD_HASHTAG',
	id
});
export const liquidSearchRemoveHashtag = id => ({
	type: 'LIQUID_SEARCH_REMOVE_HASHTAG',
	id
});
export const liquidSearchAddFlavor = id => ({
	type: 'LIQUID_SEARCH_ADD_FLAVOR',
	id
});
export const liquidSearchRemoveFlavor = id => ({
	type: 'LIQUID_SEARCH_REMOVE_FLAVOR',
	id
});
//	localization
export const setLocale = lang => ({
	type: 'SET_LOCALE',
	lang
});
//	fetch stuff
export const getAllVendors = () =>
	dispatch =>
		Api.request('vendors.list')
			.then(({list}) => dispatch(addVendors(list)));

export const getAllFlavors = () =>
	dispatch =>
		Api.request('flavors.list')
			.then(({list}) => dispatch(addFlavors(list)));

//	auth
export const setUserData = data => ({
	type: 'USER_DATA',
	data
});
export const getUserData = () =>
	dispatch =>
		Api.request('user.data')
			.then(({data}) => dispatch(setUserData(data)));
export const updateNickname = nickname => ({
	type: 'UPDATE_NICK_NAME',
	nickname
});
export const saveNickname = nickname =>
	dispatch =>
		Api.request('user.saveNickname', {nickname})
			.then(({data}) => dispatch(setUserData(data)));
