import * as locales from '../locales';

const defaultSettings = {
	locale: 'en'
};

if (typeof localStorage !== 'undefined') {
	if (typeof localStorage.locale === 'string' && Object.keys(locales).includes(localStorage.locale)) {
		defaultSettings.locale = localStorage.locale;
	}
}

export default (store = defaultSettings, action) => {
	switch (action.type) {
		case 'SET_LOCALE':
			if (typeof localStorage !== 'undefined') {
				localStorage.locale = action.lang;
			}

			return {...store, locale: action.lang};
		default:
			return store;
	}
};
