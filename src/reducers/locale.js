import * as locales from '../locales';

export default (store = locales.en, action) => {
	switch (action.type) {
		case 'SET_LOCALE':
			return locales[action.lang];
		default:
			return store;
	}
}
