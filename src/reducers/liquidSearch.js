const defaultState = {
	selectedHashtags: [],
	selectedFlavors: [],
	liquidName: '',
	textFilter: '',
	searching: false,
	results: []
};
const liquidSearch = (state = defaultState, action) => {
	switch (action.type) {
		case 'LIQUID_SEARCH_LIQUID_NAME':
			return {
				...state,
				liquidName: action.data
			};
		case 'LIQUID_SEARCH_TYPE_HINT':
			return {
				...state,
				textFilter: action.data
			};
		case 'LIQUID_SEARCH_START_SEARCH':
			return {
				...state,
				searching: true
			};
		case 'LIQUID_SEARCH_STOP_SEARCH':
			return {
				...state,
				searching: false,
				results: action.data
			};
		case 'LIQUID_SEARCH_TOGGLE_HASHTAG':
			return {
				...state,
				selectedHashtags: state.selectedHashtags.includes(action.id)
					? state.selectedHashtags.filter(id => id !== action.id)
					: [...state.selectedHashtags, action.id]
			};
		case 'LIQUID_SEARCH_ADD_HASHTAG':
			return {
				...state,
				selectedHashtags: [...state.selectedHashtags, action.id]
			};
		case 'LIQUID_SEARCH_REMOVE_HASHTAG':
			return {
				...state,
				selectedHashtags: state.selectedHashtags.filter(id => id !== action.id)
			};
		case 'LIQUID_SEARCH_ADD_FLAVOR':
			return {
				...state,
				textFilter: '',
				selectedFlavors: [...state.selectedFlavors, action.id]
			};
		case 'LIQUID_SEARCH_REMOVE_FLAVOR':
			return {
				...state,
				selectedFlavors: state.selectedFlavors.filter(id => id !== action.id)
			};
		default:
			return state
	}
};

export default liquidSearch
