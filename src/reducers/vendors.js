const vendors = (state = {}, action) => {
	switch (action.type) {
		case 'ADD_VENDOR':
			const vendor = action.data;
			return {
				...state,
				[vendor.id]: vendor
			};
		case 'ADD_VENDORS':
			const vendors = action.data.reduce((p, c) => ({...p, [c.id]: c}), {});
			return {
				...state,
				...vendors
			};
		default:
			return state
	}
};

export default vendors
