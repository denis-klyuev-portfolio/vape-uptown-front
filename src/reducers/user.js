const defaultState = {
	data: null
};

export default (state = defaultState, action) => {
	switch (action.type) {
		case 'USER_DATA':
			return {
				...state,
				data: action.data
			};
		case 'UPDATE_NICK_NAME':
			return {
				...state,
				data: {
					...state.data,
					nickname: action.nickname
				}
			};
		default:
			return state;
	}
}
