import {combineReducers} from 'redux';
import flavors from './flavors';
import vendors from './vendors';
import liquidSearch from './liquidSearch';
import locale from './locale';
import settings from './settings';
import user from './user';

const vapeApp = combineReducers({
	flavors,
	vendors,
	liquidSearch,
	locale,
	settings,
	user
});

export default vapeApp;
