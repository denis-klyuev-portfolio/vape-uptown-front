const flavors = (state = [], action) => {
	switch (action.type) {
		case 'ADD_FLAVOR':
			return [
				...state,
				action.data
			];
		case 'ADD_FLAVORS':
			return [
				...state,
				...action.data
			];
		default:
			return state
	}
};

export default flavors
