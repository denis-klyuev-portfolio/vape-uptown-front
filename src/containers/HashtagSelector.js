import classnames from 'classnames';
import {connect} from 'react-redux';
import {liquidSearchAddHashtag, liquidSearchRemoveHashtag} from '../actions';
import SelectorView from '../components/SelectorView';

const mapStateToProps = (state) => ({
	state: state
});

const mergeProps = (stateProps, dispatchProps) => {
	const state = stateProps.state;
	const dispatch = dispatchProps.dispatch;
	const getIncludes = (id) => {
		return state.liquidSearch.selectedHashtags.includes(Number(id));
	};

	return {
		label: state.liquidSearch.selectedHashtags.length
			? state.liquidSearch.selectedHashtags.map(id => `#${state.locale.hashtags[id]}`).join(' ')
			: state.locale.liquidSearchPage.hashtagsInputHint,
		list: Object.entries(state.locale.hashtags),
		onClick: (e, name, id) => {
			e.preventDefault();
			return dispatch(getIncludes(id) ? liquidSearchRemoveHashtag(id) : liquidSearchAddHashtag(id))
		},
		getClassName: (name, id) => {
			return classnames({checked: getIncludes(id)})
		}
	};
};

const HashtagSelector = connect(
	mapStateToProps,
	null,
	mergeProps
)(SelectorView);

export default HashtagSelector;
