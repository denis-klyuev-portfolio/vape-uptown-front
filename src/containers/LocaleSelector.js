import classnames from 'classnames';
import {connect} from 'react-redux';
import * as locales from '../locales';
import {setLocale} from '../actions';
import SelectorView from '../components/SelectorView';

const mapStateToProps = (state) => ({
	state: state
});

const mergeProps = (stateProps, dispatchProps) => {
	const state = stateProps.state;
	const dispatch = dispatchProps.dispatch;

	return {
		label: state.settings.locale,
		list: Object.keys(locales).map((item, index)=>[index, item]),
		onClick: (e, name) => {
			return dispatch(setLocale(name));
		},
		getClassName: (name) => {
			return classnames({checked: name === state.settings.locale})
		}
	};
};

const LocaleSelector = connect(
	mapStateToProps,
	null,
	mergeProps
)(SelectorView);

export default LocaleSelector;
