import React from 'react';

export default ({location}) => (
	<div>
		<h2>Мы не смогли отыскать эту страницу: {location.pathname}</h2>
	</div>
);
