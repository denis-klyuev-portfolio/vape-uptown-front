import React, {Component} from 'react';
import classnames from 'classnames';
import {Link} from 'react-router-dom';
import numeral from 'numeral';
import {connect} from 'react-redux';

import {Button, Form, Input, Panel} from 'muicss/react';
import './LiquidSearchPage.scss';
import Flavor from '../components/Flavor';
import HashtagSelector from '../containers/HashtagSelector';
import Api from '../Api';
import {
	liquidSearchLiquidName, liquidSearchFlavorName,
	liquidSearchAddFlavor, liquidSearchRemoveFlavor,
	liquidSearchStartSearch, liquidSearchStopSearch,
	liquidSearchToggleHashtag
} from '../actions';

class LiquidSearchPage extends Component {
	doSearch() {
		const liquidSearch = this.props.liquidSearch;
		Api.request('liquids.search', {
			name: liquidSearch.liquidName,
			flavors: liquidSearch.selectedFlavors,
			hashtags: liquidSearch.selectedHashtags
		})
			.then(({list: results}) => {
				setTimeout(() => this.props.dispatch(liquidSearchStopSearch(results)), 100);
			});
		this.props.dispatch(liquidSearchStartSearch());
	}

	render() {
		const {flavorList, vendors, liquidSearch, locale, dispatch} = this.props;
		const flavors = flavorList.reduce((p, c) => ({...p, [c.id]: c}), {});
		const guessedFlavorList = !!liquidSearch.textFilter
			? flavorList.filter(flavor => flavor.name.toLowerCase().indexOf(liquidSearch.textFilter.toLowerCase()) !== -1)
			: [];

		return (
			<div className="liquid-search-page">
				<h2>{locale.liquidSearchPage.pageTitle}</h2>

				<div className={classnames('curtain', {visible: liquidSearch.searching})}>
					<svg width="100px" height="100px">
						<circle cx={50} cy={50} r={40}/>
						{(() => {
							let result = [];
							for (let a = 0; a < 12; a++) {
								const x = Math.cos(a * Math.PI / 6) * 30;
								const y = Math.sin(a * Math.PI / 6) * 30;

								result.push(
									<circle cx={50 + x} cy={50 + y} r={4} opacity={(a + 1) / 12} key={a}/>
								);
							}
							return result;
						})()}
					</svg>
				</div>
				<Form inline={true}>
					<HashtagSelector/>
					<Input
						label={locale.liquidSearchPage.liquidNameInputHint}
						floatingLabel={true}
						value={liquidSearch.liquidName}
						onChange={e => dispatch(liquidSearchLiquidName(e.target.value))}/>
					<Input
						label={locale.liquidSearchPage.flavorNameInputHint}
						floatingLabel={true}
						value={liquidSearch.textFilter}
						onChange={e => dispatch(liquidSearchFlavorName(e.target.value))}/>
					{!!guessedFlavorList.length &&
					<div className="flavor-list">
						{guessedFlavorList.map(flavor =>
							<div
								key={`flavor-filter-guess-${flavor.id}`}
								onClick={() => dispatch(liquidSearchAddFlavor(flavor.id))}
								style={{cursor: 'pointer'}}
								title="Click to add"
							>
								<Flavor {...flavor} vendor={vendors[flavor.vendorId]} clickable={false}/>
							</div>)}
					</div>}
					{!!guessedFlavorList.length && <hr/>}
					<div className="flavor-list">
						{liquidSearch.selectedFlavors.map(flavorId =>
							<div key={`flavor-filter-${flavorId}`}
								 onClick={() => dispatch(liquidSearchRemoveFlavor(flavorId))}
								 style={{cursor: 'pointer'}}
								 title="Click to remove"
							>
								<Flavor {...flavors[flavorId]} vendor={vendors[flavors[flavorId].vendorId]}
										clickable={false}/>
							</div>)}
					</div>
					<Button
						variant="raised"
						color="primary"
						onClick={(e) => {
							e.preventDefault();
							this.doSearch();
						}}
					>
						{locale.liquidSearchPage.searchButton}
					</Button>
				</Form>

				<h2>{locale.liquidSearchPage.resultsTitle}</h2>

				{liquidSearch.results.length ?
					<div className="flavor-list">
						{liquidSearch.results.map(liquid => <Panel key={`liquid-${liquid.id}`}>
							<h3><Link to={`/liquid/${liquid.id}`}>{liquid.name}</Link></h3>
							<div>
								Flavor: {numeral(liquid.flavors.reduce((p, c) => p + c.volume, 0) / 100).format('0[.]0')}%
							</div>
							<div>
								{liquid.hashtags.map(id => <b style={{cursor: 'pointer'}} key={id} onClick={() => dispatch(liquidSearchToggleHashtag(id))}>#{locale.hashtags[id]} </b>)}
							</div>
						</Panel>)}
					</div>
					: <div>{locale.liquidSearchPage.emptyResultsTitle}</div>
				}
			</div>
		);
	}
}

export default connect(
	state => ({
		vendors: state.vendors,
		flavorList: state.flavors,
		liquidSearch: state.liquidSearch,
		locale: state.locale
	})
)(LiquidSearchPage);
