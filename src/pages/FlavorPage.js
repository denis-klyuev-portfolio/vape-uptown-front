import React from 'react';
import {connect} from 'react-redux';

const FlavorDetailsPresentation = ({vendors, flavorList, match: {params: {flavorId}}}) => {
	const flavors = flavorList.reduce((p, c) => ({...p, [c.id]: c}), {});
	const flavor = flavors[flavorId] || null;
	const vendor = flavor ? vendors[flavor.vendorId] : null;

	return (
		<div>
			<h1>Аромка "{!!flavor && flavor.name}"</h1>
			<div>
				Описание вендора "{!!vendor && vendor.name}"
			</div>
		</div>
	);
};

export default connect(
	state => ({
		vendors: state.vendors,
		flavorList: state.flavors
	})
)(FlavorDetailsPresentation);

