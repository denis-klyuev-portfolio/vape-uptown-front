import React from 'react';
import {connect} from 'react-redux';

import './FlavorListPage.scss';
import FlavorList from '../components/FlavorList';

const FlavorListContainer = connect(
	state => ({
		vendors: state.vendors,
		flavors: state.flavors
	})
)(FlavorList);

const FlavorListPage = () => (
	<div className="flavor-list-page">
		<h1>Flavor list</h1>
		<FlavorListContainer/>
	</div>
);

export default FlavorListPage;
