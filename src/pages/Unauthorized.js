import React from 'react';
import {connect} from 'react-redux';

const Unauthorized = ({location, locale}) => (
	<div>
		<h2>{locale.unauthorizedPageTitle}</h2>
	</div>
);

export default connect(state => ({
	locale: state.locale
}))(Unauthorized);
