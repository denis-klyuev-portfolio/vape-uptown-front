import React from 'react';
import {connect} from 'react-redux';
import {Button, Form, Input} from 'muicss/react';
import {updateNickname, saveNickname} from '../../actions';

const ProfilePage = ({user, locale, updateNickname, saveNickname}) => (
	<div>
		<h1>{locale.profile.indexPageTitle}</h1>
		<div>
			<Form
				onSubmit={e => {e.preventDefault(); saveNickname()}}>
				<Input
					floatingLabel={true}
					label={locale.profile.editNicknamePlaceholder}
					onChange={e => updateNickname(e.target.value)}
					value={user && user.nickname !== null ? user.nickname : ''}
				/>
				<Button color="primary" variant="raised">{locale.form.saveButton}</Button>
			</Form>
		</div>
	</div>
);

export default connect(state => ({
	user: state.user.data,
	locale: state.locale
}), dispatch => ({dispatch}), (stateProps, {dispatch}) => ({
	...stateProps,
	saveNickname: () => {
		const {nickname} = stateProps.user;
		console.log(`predispatch saveNickname: ${nickname}`);
		dispatch(saveNickname(nickname))
	},
	updateNickname: nickname => dispatch(updateNickname(nickname))
}))(ProfilePage);
