import React from 'react';
import {connect} from 'react-redux';

const FavoritesPage = ({user, locale}) => (
	<div>
		<h1>{locale.profile.favoriteRecipesPageTitle}</h1>
	</div>
);

export default connect(state => ({
	user: state.user,
	locale: state.locale
}))(FavoritesPage);
