import React from 'react';
import {
	Link,
	Route,
	Switch
} from 'react-router-dom';
import {connect} from 'react-redux';
import {Panel} from 'muicss/react';

import ProfileIndexPage from './profile/index';
import MyRecipesPage from './profile/myRecipes';
import FavoriteRecipesPage from './profile/favoriteRecipes';

const Profile = ({user, locale}) => (
	<div className="profile-page">
		<Panel>
			<ul>
				<Link to="/profile">{locale.profile.indexPageTitle}</Link>
				{' | '}
				<Link to="/profile/favorite-recipes">{locale.profile.favoriteRecipesPageTitle}</Link>
				{' | '}
				<Link to="/profile/my-recipes">{locale.profile.myRecipesPageTitle}</Link>
			</ul>
			<Switch>
				<Route exact path="/profile" component={ProfileIndexPage}/>
				<Route exact path="/profile/favorite-recipes" component={FavoriteRecipesPage}/>
				<Route exact path="/profile/my-recipes" component={MyRecipesPage}/>
			</Switch>
		</Panel>
	</div>
);

export default connect(state => ({
	user: state.user,
	locale: state.locale
}))(Profile);
