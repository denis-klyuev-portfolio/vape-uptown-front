import React, {Component} from 'react';
import numeral from 'numeral';
import classnames from 'classnames';
import {connect} from 'react-redux';

import Api from '../Api';
import './LiquidPage.scss';
import Bottle from '../components/Bottle';
import {Button, Panel} from 'muicss/react';

class LiquidDetails extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searching: true,
			liquidId: props.match.params.liquidId,
			totalVolume: 60,
			item: null
		};
		this.handleVolumeChange = this.handleVolumeChange.bind(this);
		this.getData();
	}

	getData() {
		if (!this.state.searching) {
			this.setState({
				searching: true
			});
		}
		Api.request('liquids.oneById', {id: this.state.liquidId})
			.then(({item}) =>
				this.setState({
					searching: false,
					item
				})
			);
	}

	handleVolumeChange(volume) {
		this.setState({
			totalVolume: volume
		});
	}

	render() {
		const {vendors, flavorList, locale} = this.props;
		const flavors = flavorList.reduce((p, c) => ({...p, [c.id]: c}), {});
		const liquid = this.state.item;
		const dataIsReady = !!(flavorList.length && Object.keys(vendors).length && liquid);

		if (!dataIsReady) {
			return false;
		}

		const totalFlavors = liquid.flavors.map(lFlavor => {
				const flavor = flavors[lFlavor.id];
				const vendor = vendors[flavor.vendorId];
				return {
					id: lFlavor.id,
					volume: lFlavor.volume,
					flavor, vendor
				};
			}
		);

		const totalFlavorPercent = totalFlavors.reduce((p, {volume}) => p + volume, 0);
		const measure = locale.ml;//'💧'

		const {vgPart, adPart, nicPart} = liquid;
		const pgPart = 10000 - vgPart - nicPart - adPart - totalFlavorPercent;

		return (
			<div className="liquid-page">
				<h2>{liquid.name}</h2>

				<Panel>
					<ul className="mui-tabs__bar">
						{[30, 60, 100].map((volume, index) => (
							<li className={classnames({'mui--is-active': this.state.totalVolume === volume})}
								key={index}><a
								data-mui-toggle="tab" style={{cursor: 'pointer'}}
								onClick={() => this.handleVolumeChange(volume)}>{volume}{locale.ml}</a></li>
						))}
					</ul>
					<table>
						<tbody>
						<tr>
							<td>
								<Bottle
									vg={this.state.totalVolume * vgPart / 10000}
									pg={this.state.totalVolume * pgPart / 10000}
									ad={this.state.totalVolume * adPart / 10000}
									nic={this.state.totalVolume * nicPart / 10000}
									fl={this.state.totalVolume * totalFlavorPercent / 10000}
									measure={measure}/>
								<div>
									<h3>{locale.liquidPage.finalProportionsTitle}</h3>
									PG/VG/AD/FL: {100 - (vgPart + adPart)/100}/{vgPart/100}/{adPart/100}/{totalFlavorPercent/100}
								</div>
							</td>
							<td>
								<table className="flavor-list">
									<thead>
									<tr>
										<th>{locale.liquidPage.details.flavor}</th>
										<th>%</th>
										<th>{locale.liquidPage.details.volume}</th>
										<th>{locale.liquidPage.details.drops}</th>
									</tr>
									</thead>
									<tbody>
									{totalFlavors.map(({id, volume, flavor, vendor}) => (
										<tr key={`flavor-${id}`} className="flavor">
											<td>
												<div className="flavor-name">{flavor.name}</div>
												<div className="vendor-name">{vendor.name}</div>
											</td>
											<td>{numeral(volume / 100).format('0[.]0')}%</td>
											<td>{numeral(this.state.totalVolume * volume / 10000).format('0[.]0')} {locale.ml}
											</td>
											<td>{numeral(this.state.totalVolume * volume / 10000 * 33).format('0')}</td>
										</tr>
									))}
									</tbody>
								</table>
							</td>
						</tr>
						</tbody>
					</table>
					{!!liquid.description && <div className="description">{liquid.description}</div>}
					{!!liquid.bestInDays && <div>Настаивать {liquid.bestInDays} дней</div>}
					<div>
						{liquid.hashtags.map(id => <b key={id}>#{locale.hashtags[id]} </b>)}
					</div>
					<Button
						variant="raised"
						color="primary"
						onClick={(e) => {
							e.preventDefault();
						}}
					>
						{locale.print}
					</Button>
				</Panel>
			</div>
		)
			;
	}
}

export default connect(
	state => ({
		vendors: state.vendors,
		flavorList: state.flavors,
		locale: state.locale
	})
)(LiquidDetails);

