import {getAllVendors, getAllFlavors, getUserData, setLocale} from './actions';
import thunk from 'redux-thunk';
import vapeApp from './reducers';
import {applyMiddleware, createStore} from 'redux';

/* eslint-disable no-underscore-dangle */
const middlewares = [thunk];
const reduxDevtoolsExt = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = createStore(vapeApp, reduxDevtoolsExt, applyMiddleware(...middlewares));
/* eslint-enable */

store.start = () => {
	store.dispatch(setLocale(store.getState().settings.locale));
	store.dispatch(getAllVendors());
	store.dispatch(getAllFlavors());
	store.dispatch(getUserData());
};

export default store;
