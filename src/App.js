import React from 'react';

import './App.scss';
import LiquidSearchPage from './pages/LiquidSearchPage';
import LiquidPage from './pages/LiquidPage';
import FlavorPage from './pages/FlavorPage';
import FlavorListPage from './pages/FlavorListPage';
import NotFoundPage from './pages/NotFoundPage';
import ProfilePage from './pages/Profile';
import UnauthorizedPage from './pages/Unauthorized';
import TopBar from './components/TopBar';
import {
	BrowserRouter as Router,
	Route,
	Switch
} from 'react-router-dom';

const App = () => (
	<Router>
		<div id="page">
			<TopBar/>
			<div id="contents">
				<Switch>
					<Route exact path="/" component={LiquidSearchPage}/>
					<Route path="/profile" component={ProfilePage}/>
					<Route path="/liquid/:liquidId" component={LiquidPage}/>
					<Route path="/flavor/:flavorId" component={FlavorPage}/>
					<Route path="/flavor-list" component={FlavorListPage}/>
					<Route path="/profile" component={ProfilePage}/>
					<Route path="/unauthorized" component={UnauthorizedPage}/>
					<Route component={NotFoundPage}/>
				</Switch>
			</div>
		</div>
	</Router>
);

export default App;
